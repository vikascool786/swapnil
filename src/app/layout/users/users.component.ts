import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    userResponse: any;  userLists: any;
    profileImgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/users_imgs/';
    closeResult: string;
    formTitle: string;
    category: any = {name: '', description: ''};
    files: any;
    modalReference: NgbModalRef;

    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getuserLists();
    }


    open(content) {
        this.formTitle = 'New Category';
        this.modalReference = this.modalService.open(content);
        this.modalReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    // delete users list by id
    deleteUser(id) {
        alert(id);
    }
    // get all users list
    getuserLists() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/getAllUsers';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.userResponse = response.json();
            this.userLists = this.userResponse.response;
        });

    }


}
