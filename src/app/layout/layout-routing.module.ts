import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'sub-category', loadChildren: './sub-category/sub-category.module#SubCategoryModule' },
            { path: 'users', loadChildren: './users/users.module#UsersModule' },
            { path: 'services', loadChildren: './services/services.module#ServicesModule' },
            { path: 'customer-reviews', loadChildren: './customer-reviews/customer-reviews.module#CustomerReviewsModule' },
            // orderbolo paths
            { path: 'orders', loadChildren: './orders/orders.module#OrdersModule' },
            { path: 'user-documents', loadChildren: './user-documents/user-documents.module#UserDocumentsModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
