import { Component, OnInit, ElementRef  } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import {  ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// services
import { GeneralService } from '../../services/general.service';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {
    categoryList1: any;  categoryList: any;
    imgUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/assets/uploads/category_imgs/';
    closeResult: string;
    formTitle: string;
    category: any = {name: '', description: ''};
    files: any;
    constructor(
        public http: Http,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        public elem: ElementRef,
        private GeneralService: GeneralService) {
    }
    onSelectedFile(event) {
        this.files = event.target.files;
          console.log(this.files);
        }

    ngOnInit() {
        this.getCategoryList();
    }


    open(content) {
        this.formTitle = 'New Category';
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    addCategory() {
    const files = this.files;
    const formData = new FormData();
    formData.append('user_id', '1');
    formData.append('access_token', 'vbJ9IjocNQT8MjM5MWxaLXJaIjogMQF9NQIaOQZ3M7MbDG4awQAzwD8NymlIz3TaB3hew70AREwoTaSlTCJox7A8yHpbBlSUQjAN');
    formData.append('name', this.category.name);
    formData.append('description', this.category.description);
    for (let j = 0; j < files.length; j++) {
            formData.append('file', files[j], files[j].name);
    }
    this.GeneralService.insertData(formData).subscribe(res => {
        if (res.json().status === 'success') {
            this.getCategoryList();
        }
      });
    }

    getCategoryList() {
        const sampleUrl = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/categoryList';
        this.http.get(sampleUrl)
        .subscribe((response) => {
            this.categoryList1 = response.json();
            this.categoryList = this.categoryList1.response.categories;
        });

    }


}
