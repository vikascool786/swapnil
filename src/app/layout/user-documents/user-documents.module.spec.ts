import { UserDocumentsModule } from './user-documents.module';

describe('UserDocumentsModule', () => {
    let userDocumentsModule: UserDocumentsModule;

    beforeEach(() => {
        userDocumentsModule = new UserDocumentsModule();
    });

    it('should create an instance', () => {
        expect(userDocumentsModule).toBeTruthy();
    });
});
