import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  Url: any = 'http://vitsolutions24x7.com/orderbolo/api/api/ProjectController/';

  constructor(private http: Http) {}

  public insertData(formdata: any ) {
    const _url: string = this.Url + 'addCategory';
    return this.http.post(_url, formdata)
    .catch(this._errorHandler) ;
  }

    private _errorHandler(error: Response) {
      console.error('Error Occured: ' + error);
      return Observable.throw(error || 'Some Error on Server Occured');
    }


  //   public  getFilesList(id:string) {
  //     let data = {id:id}
  //     let _url: string = this.Url+'sub_folder_files.php';
  //     return this.http.get(_url, {params: data});
  //   }

  // delete record with attachment
  public  deleteRecordByFile(id: string) {
    const data = {id: id};
    const _url: string = this.Url + 'delete_sub_folder_single_file.php';
    return this.http.get(_url, {params: data});
  }

  // delete record without attachment
  public  deleteRecordById(id: string) {
    const data = {id: id};
    const _url: string = this.Url + 'delete_sub_folder_single_file.php';
    return this.http.get(_url, {params: data});
  }



}
